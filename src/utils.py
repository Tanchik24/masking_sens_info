import os
from typing import List

from src.config import Config


def created_folders(path: str) -> None:
    os.makedirs(path, exist_ok=True)


def make_file(file_path: str, content: str) -> None:
    with open(file_path, "w") as file:
        file.write(content)


def get_data_from_file(file_path: str) -> List[str]:
    with open(file_path, "r") as file:
        return file.readlines()


def get_entities() -> List[str]:
    with open(os.path.join(Config.INTERIM_DATA_DIR, Config.ENTITIES_FILE_NAME)) as f:
        entities = [entity.replace("\n", "") for entity in f.readlines()]
        return entities


def get_params() -> dict:
    return {
        "WEIGHT_DECAY": Config.WEIGHT_DECAY,
        "LEARNING_RATE": Config.LEARNING_RATE,
        "BATCH_SIZE": Config.BATCH_SIZE,
        "ADAM_EPSILON": Config.ADAM_EPSILON,
        "GRADIENT_ACCUMULATION_STEPS": Config.GRADIENT_ACCUMULATION_STEPS,
        "NUM_TRAIN_EPOCHS": Config.NUM_TRAIN_EPOCHS,
        "WARMUP_STEPS": Config.WARMUP_STEPS,
        "MAX_GRAD_NORM": Config.MAX_GRAD_NORM,
        "DROPOUT_RATE": Config.DROPOUT_RATE,
    }
