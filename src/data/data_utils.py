from typing import List, Tuple

import pandas as pd


def get_text_entities(file_path: str) -> Tuple[List[List[str]], List[List[str]]]:
    df = pd.read_csv(file_path)
    texts = [text.split() for text in df["1"].tolist()]
    entities = [text.split() for text in df["0"].tolist()]
    return texts, entities


def get_unique_entities(entities: List[str]) -> List[str]:
    return list(set(entities))


def convert_entities_to_labels(
    entities: List[List[str]], unique_entities: List[str], pad_label_id: int
) -> List[List[int]]:
    entities_labels = []
    for entities_list in entities:
        sentence_entity_label = [
            unique_entities.index(entity) if entity in unique_entities else pad_label_id
            for entity in entities_list
        ]
        entities_labels.append(sentence_entity_label)
    return entities_labels
