import click

from src import logger
from src.data import raw_data_preprocessor


@click.command()
def process_raw_data():
    logger.info("************* Preparing raw data *************")

    logger.info("Train stage data preprocessing ...")
    logger.info("<====================================>")
    raw_data_preprocessor.preprocess_raw_data("train")

    logger.info("Validation stage data preprocessing ...")
    logger.info("<====================================>")
    raw_data_preprocessor.preprocess_raw_data("test")


if __name__ == "__main__":
    process_raw_data()
