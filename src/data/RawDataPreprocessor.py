import os
from pathlib import Path
from typing import List

from src import logger
from src.config import Config
from src.data import (convert_entities_to_labels, get_text_entities,
                      get_unique_entities)
from src.utils import created_folders, make_file


class RawDataPreprocessor:
    interim_data_path: Path

    def __init__(self):
        self.unique_entities: List[str] = []
        self.raw_data_path = Config.RAW_DATA_DIR
        self.interim_data_path = Config.INTERIM_DATA_DIR
        self.processed_data_path = Config.PROCESSED_DATA_DIR

    def make_unique_entities_file(self, entities: List[str]) -> None:
        self.unique_entities = get_unique_entities(entities)
        entities_str = "\n".join(self.unique_entities)
        file_path = os.path.join(self.interim_data_path, Config.ENTITIES_FILE_NAME)
        make_file(file_path, entities_str)

    def preprocess_raw_data(self, stage: str):
        stage_path = os.path.join(self.interim_data_path, stage)
        created_folders(stage_path)
        logger.info(f"Created folder: {stage_path}")

        logger.info("Uploading texts and entities...")
        texts, entities = get_text_entities(
            os.path.join(self.raw_data_path, f"{stage}.csv")
        )
        logger.info(f"Number of sentences: {len(texts)}")

        if stage == Config.TRAIN_FOLDER:
            logger.info("Traing stage =====> making unique entities file")
            self.make_unique_entities_file(
                [item for sublist in entities for item in sublist]
            )

        logger.info("Converting entities to labels...")
        entities_labels = convert_entities_to_labels(
            entities, self.unique_entities, Config.PAD_LABEL_ID
        )

        logger.info("Preparing data files...")
        entities_labels_str = [" ".join(map(str, entity)) for entity in entities_labels]
        texts_str = [" ".join(text) for text in texts]

        make_file(os.path.join(stage_path, Config.TEXT_FILE_NAME), "\n".join(texts_str))
        make_file(
            os.path.join(stage_path, Config.ENTITIES_FILE_NAME),
            "\n".join(entities_labels_str),
        )


raw_data_preprocessor = RawDataPreprocessor()
