import logging
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()


class Config:
    PROJ_ROOT = Path(__file__).resolve().parents[1]
    logging.info(f"PROJ_ROOT path is: {PROJ_ROOT}")

    DATA_DIR = PROJ_ROOT / "data"
    RAW_DATA_DIR = DATA_DIR / "raw"
    INTERIM_DATA_DIR = DATA_DIR / "interim"
    PROCESSED_DATA_DIR = DATA_DIR / "processed"

    MODELS_DIR = PROJ_ROOT / "models"

    REPORTS_DIR = PROJ_ROOT / "reports"
    FIGURES_DIR = REPORTS_DIR / "figures"

    TEXT_FILE_NAME = "text.txt"
    ENTITIES_FILE_NAME = "entities.txt"
    TRAIN_FOLDER = "train"

    PAD_LABEL_ID = 7
    CLS_TOKEN_SEGMENT_ID = 0
    SEQUENCE_A_SEGMENT_ID = 0
    MAX_SEQ_LENGTH = 50
    SPECIAL_TOKENS_COUNT = 2

    BERT_PRETRAINED = "./models/rubert_cased_L-12_H-768_A-12_pt_v1"
    WEIGHT_DECAY = 0.1
    LEARNING_RATE = 5e-5
    ADAM_EPSILON = 1e-8
    GRADIENT_ACCUMULATION_STEPS = 1
    NUM_TRAIN_EPOCHS = 1
    WARMUP_STEPS = 500
    MAX_GRAD_NORM = 1
    LOGGING_STEPS = 19
    SAVE_MODEL_DIR = MODELS_DIR / "NER_MODEL"
    SAVE_STEPS = 19
    BATCH_SIZE = 64
    DROPOUT_RATE = 0.2
    IGNORE_INDEX = 7

    EXPERIMENT_NAME = "bert_ner_experiments"
    MLFLOW_URL = "http://localhost:5040"
    MODEL_NAME = "test_model"
