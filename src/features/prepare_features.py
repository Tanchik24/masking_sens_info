import click
from transformers import BertTokenizer

from src import logger
from src.config import Config
from src.features.FeaturesPreprocessor import FeaturesPreprocessor


@click.command()
def prepare_features():
    logger.info("************* Loading tokeniser *************")
    tokenizer = BertTokenizer.from_pretrained(Config.BERT_PRETRAINED)

    logger.info("************* Preparing features*************")
    feature_preprocessor = FeaturesPreprocessor(tokenizer)

    logger.info("Train stage features preprocessing ...")
    logger.info("<====================================>")
    feature_preprocessor.get_features("train")

    logger.info("Valid stage features preprocessing ...")
    logger.info("<====================================>")
    feature_preprocessor.get_features("test")


if __name__ == "__main__":
    prepare_features()
