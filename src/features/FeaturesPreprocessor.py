import os
from typing import List, Tuple

import torch
from transformers import PreTrainedTokenizer

from src.config import Config
from src.features import (Feature, add_attention_mask, add_padding_to_label,
                          add_special_tokens, cut_length, get_text_entities)


class FeaturesPreprocessor:
    def __init__(self, tokenizer: PreTrainedTokenizer):
        self.tokenizer = tokenizer
        self.mask_padding_with_zero: bool = True

    def get_features(self, stage: str) -> None:
        texts, entities_list = get_text_entities(stage)
        features = []
        for text, entities in zip(texts, entities_list):
            tokens, entity_labels_ids = self.tokenize_and_align_labels(text, entities)
            tokens, entity_labels_ids = cut_length(tokens, entity_labels_ids)
            tokens, entity_labels_ids, token_type_ids = add_special_tokens(
                tokens,
                entity_labels_ids,
                self.tokenizer.sep_token,
                self.tokenizer.cls_token,
            )
            input_ids = self.tokenizer.convert_tokens_to_ids(tokens)
            (
                input_ids,
                attention_mask,
                token_type_ids,
                entity_labels_ids,
            ) = add_attention_mask(
                input_ids,
                token_type_ids,
                entity_labels_ids,
                self.tokenizer.pad_token_id,
                self.mask_padding_with_zero,
            )

            features.append(
                Feature(
                    input_ids=input_ids,
                    attention_mask=attention_mask,
                    token_type_ids=token_type_ids,
                    entity_labels_ids=entity_labels_ids,
                )
            )

        self.save_features(features, stage)

    def tokenize_and_align_labels(
        self, text: List[str], entities: List[int]
    ) -> Tuple[List[str], List[int]]:
        tokens = []
        entity_labels_ids = []
        for word, entity in zip(text, entities):
            word_tokens = self.tokenizer.tokenize(word)
            if not word_tokens:
                word_tokens = [self.tokenizer.unk_token]
            tokens.extend(word_tokens)
            entity_labels_ids.extend(add_padding_to_label(entity, len(word_tokens)))

        return tokens, entity_labels_ids

    def save_features(self, features: List[Feature], stage: str):
        torch.save(features, os.path.join(Config.PROCESSED_DATA_DIR, stage))
