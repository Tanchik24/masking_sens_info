import os
import string
from typing import Any, Dict, List, Tuple, Union

import torch
from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler,
                              TensorDataset)
from transformers import BertTokenizer

from src import logger
from src.config import Config
from src.features.Feature import Feature
from src.utils import get_data_from_file


def get_text_entities(stage_path: str) -> Tuple[List[List[str]], List[List[int]]]:
    texts = get_data_from_file(
        os.path.join(str(Config.INTERIM_DATA_DIR), stage_path, Config.TEXT_FILE_NAME)
    )
    entities_labels = get_data_from_file(
        os.path.join(
            str(Config.INTERIM_DATA_DIR), stage_path, Config.ENTITIES_FILE_NAME
        )
    )
    texts_list = [text.split() for text in texts]
    entities_labels_list = [list(map(int, text.split())) for text in entities_labels]
    return texts_list, entities_labels_list


def add_padding_to_label(entity: int, pad_len: int) -> List[int]:
    entity_label_id = [entity] + [Config.PAD_LABEL_ID] * (pad_len - 1)
    return entity_label_id


def cut_length(
    tokens: List[str],
    entity_labels_ids: List[int],
) -> Tuple[List[str], List[int]]:
    max_len = Config.MAX_SEQ_LENGTH
    special_tokens_count = Config.SPECIAL_TOKENS_COUNT
    if len(tokens) > max_len - special_tokens_count:
        tokens = tokens[: (max_len - special_tokens_count)]
        entity_labels_ids = entity_labels_ids[: (max_len - special_tokens_count)]
    return tokens, entity_labels_ids


def add_special_tokens(
    tokens: List[str],
    entity_labels_ids: List[int],
    sep_token: str,
    cls_token: str,
) -> Tuple[List[str], List[int], List[int]]:
    tokens += [sep_token]
    entity_labels_ids += [Config.PAD_LABEL_ID]
    token_type_ids = [Config.SEQUENCE_A_SEGMENT_ID] * len(tokens)

    tokens = [cls_token] + tokens
    entity_labels_ids = [Config.PAD_LABEL_ID] + entity_labels_ids
    token_type_ids = [Config.CLS_TOKEN_SEGMENT_ID] + token_type_ids

    return tokens, entity_labels_ids, token_type_ids


def add_attention_mask(
    input_ids: List[int],
    token_type_ids: List[int],
    entity_labels_ids: List[int],
    pad_token_id: int,
    mask_padding_with_zero: bool,
):
    attention_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)
    padding_length = Config.MAX_SEQ_LENGTH - len(input_ids)

    input_ids = input_ids + ([pad_token_id] * padding_length)
    attention_mask = attention_mask + (
        [0 if mask_padding_with_zero else 1] * padding_length
    )
    token_type_ids = token_type_ids + ([pad_token_id] * padding_length)
    entity_labels_ids = entity_labels_ids + ([Config.PAD_LABEL_ID] * padding_length)

    return input_ids, attention_mask, token_type_ids, entity_labels_ids


def get_dataloaders_dict() -> Dict[str, DataLoader]:
    logger.info("************* Preparing dataloaders ... *************")
    train_features = torch.load(os.path.join(Config.PROCESSED_DATA_DIR, "train"))
    test_features = torch.load(os.path.join(Config.PROCESSED_DATA_DIR, "test"))
    train_dataset = make_dataset(train_features)
    test_dataset = make_dataset(test_features)
    train_dataloader = make_dataloader(train_dataset)
    test_dataloader = make_dataloader(test_dataset, "test")
    return {"train": train_dataloader, "valid": test_dataloader}


def make_dataset(features: List[Feature]) -> TensorDataset:
    all_input_ids = torch.tensor([f.input_ids for f in features], dtype=torch.long)
    all_attention_mask = torch.tensor(
        [f.attention_mask for f in features], dtype=torch.long
    )
    all_token_type_ids = torch.tensor(
        [f.token_type_ids for f in features], dtype=torch.long
    )
    all_slot_labels_ids = torch.tensor(
        [f.entity_labels_ids for f in features], dtype=torch.long
    )

    dataset = TensorDataset(
        all_input_ids, all_attention_mask, all_token_type_ids, all_slot_labels_ids
    )
    return dataset


def make_dataloader(dataset: TensorDataset, stage: str = "train") -> DataLoader:
    sampler: Union[RandomSampler, SequentialSampler]
    if stage == "train":
        sampler = RandomSampler(dataset)
    else:
        sampler = SequentialSampler(dataset)

    dataloader = DataLoader(dataset, sampler=sampler, batch_size=Config.BATCH_SIZE)

    return dataloader


def get_tokens(tokenizer: BertTokenizer) -> Dict[str, Any]:
    return {
        "cls_token": tokenizer.cls_token,
        "sep_token": tokenizer.sep_token,
        "unk_token": tokenizer.unk_token,
        "pad_token_id": tokenizer.pad_token_id,
    }


def prepare_lists_for_model_input() -> Dict[str, List[List[int]]]:
    return {
        "all_input_ids": [],
        "all_attention_mask": [],
        "all_token_type_ids": [],
        "all_entity_label_mask": [],
    }


def clean_message(text: str) -> str:
    return text.translate(str.maketrans("", "", string.punctuation)).lower()


def tokenize_and_mask_sentence(
    sentence: List[str], tokenizer: BertTokenizer, tokens_dict: Dict[str, Any]
):
    tokens = []
    entity_label_mask = []
    for word in sentence:
        word_tokens = tokenizer.tokenize(word)
        if not word_tokens:
            word_tokens = [tokens_dict["unk_token"]]
        tokens.extend(word_tokens)
        entity_label_mask.extend(
            [Config.PAD_LABEL_ID + 1] + [Config.PAD_LABEL_ID] * (len(word_tokens) - 1)
        )
    return tokens, entity_label_mask
