from typing import List

import torch
from torch.utils.data import TensorDataset
from transformers import BertTokenizer

from src.features import (add_attention_mask, add_special_tokens,
                          clean_message, cut_length, get_tokens,
                          prepare_lists_for_model_input,
                          tokenize_and_mask_sentence)


def prepare_data_for_predict(
    sentences: List[str],
    tokenizer: BertTokenizer,
    mask_padding_with_zero: bool = True,
) -> TensorDataset:
    tokens_dict = get_tokens(tokenizer)
    model_input_lists = prepare_lists_for_model_input()
    sentences_list = [clean_message(sentence).split(" ") for sentence in sentences]
    sentences_list = [[word for word in text if word != ""] for text in sentences_list]
    for sentence in sentences_list:
        tokens, entity_label_mask = tokenize_and_mask_sentence(
            sentence, tokenizer, tokens_dict
        )
        tokens, entity_label_mask = cut_length(tokens, entity_label_mask)
        tokens, entity_label_mask, token_type_ids = add_special_tokens(
            tokens,
            entity_label_mask,
            tokens_dict["sep_token"],
            tokens_dict["cls_token"],
        )
        input_ids = tokenizer.convert_tokens_to_ids(tokens)
        input_ids, attention_mask, token_type_ids, entity_label_mask = (
            add_attention_mask(
                input_ids,
                token_type_ids,
                entity_label_mask,
                tokens_dict["pad_token_id"],
                mask_padding_with_zero=mask_padding_with_zero,
            )
        )

        model_input_lists["all_input_ids"].append(input_ids)
        model_input_lists["all_attention_mask"].append(attention_mask)
        model_input_lists["all_token_type_ids"].append(token_type_ids)
        model_input_lists["all_entity_label_mask"].append(entity_label_mask)

    all_input_ids_tensor = torch.tensor(
        model_input_lists["all_input_ids"], dtype=torch.long
    )
    all_attention_mask_tensor = torch.tensor(
        model_input_lists["all_attention_mask"], dtype=torch.long
    )
    all_token_type_ids_tensor = torch.tensor(
        model_input_lists["all_token_type_ids"], dtype=torch.long
    )
    all_entity_label_mask_tensor = torch.tensor(
        model_input_lists["all_entity_label_mask"], dtype=torch.long
    )

    dataset = TensorDataset(
        all_input_ids_tensor,
        all_attention_mask_tensor,
        all_token_type_ids_tensor,
        all_entity_label_mask_tensor,
    )
    return dataset
