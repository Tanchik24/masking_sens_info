import click
from transformers import BertTokenizer

from src.config import Config
from src.models.Predictor import Predictor


@click.command()
@click.option("--text", required=True, help="Text", type=click.STRING)
def predict(text: str):
    model = Predictor.load_model()
    tokenizer = BertTokenizer.from_pretrained(Config.BERT_PRETRAINED)
    predictor = Predictor(model, tokenizer)
    text_list = [elem for elem in text.split(".") if elem != ""]
    entity_labels, entity_probs = predictor.predict(text_list)
    print(entity_labels)


if __name__ == "__main__":
    predict()
