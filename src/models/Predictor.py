import os
from typing import List

import numpy as np
import torch
from tqdm import tqdm
from transformers import BertTokenizer

from src import logger
from src.config import Config
from src.features import make_dataloader, prepare_data_for_predict
from src.models import accumulate_entity_predictions, get_device
from src.models.EntityExtractionBERT import EntityExtractionBERT
from src.utils import get_entities


class Predictor:
    def __init__(self, model: EntityExtractionBERT, tokenizer: BertTokenizer):
        self.model = model
        self.tokenizer = tokenizer
        self.device = get_device()
        self.model.to(self.device)
        self.entities = get_entities()
        self.pad_token_label_id = Config.PAD_LABEL_ID
        self.model.eval()

    def batch_step(self, batch):
        batch = tuple(t.to(self.device) for t in batch)
        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "token_type_ids": batch[2],
            }

            return self.model(**inputs)

    def _process_predictions(
        self, entity_preds: np.ndarray, all_entity_label_mask: np.ndarray
    ) -> List[List[str]]:
        entity_preds = np.argmax(entity_preds, axis=2)
        entity_labels = self._decode_slot_predictions(
            entity_preds, all_entity_label_mask
        )

        return entity_labels

    def _decode_slot_predictions(
        self, entity_preds: np.ndarray, all_entity_label_mask: np.ndarray
    ) -> List[List[str]]:
        slot_label_map = {i: label for i, label in enumerate(self.entities)}
        slot_preds_list: List[List[str]] = [[] for _ in range(entity_preds.shape[0])]

        for i in range(entity_preds.shape[0]):
            for j in range(entity_preds.shape[1]):
                if all_entity_label_mask[i, j] != self.pad_token_label_id:
                    slot_preds_list[i].append(slot_label_map[entity_preds[i][j]])

        return slot_preds_list

    def predict(self, sentences: List[str]):
        dataset = prepare_data_for_predict(sentences, self.tokenizer)
        dataloader = make_dataloader(dataset, "test")
        entity_preds: np.ndarray = np.empty((0, 0))
        all_entity_label_mask: np.ndarray = np.empty((0, 0))
        for batch in tqdm(dataloader, desc="Predicting"):
            output = self.batch_step(batch)
            entity_preds, all_entity_label_mask = accumulate_entity_predictions(
                output, batch, entity_preds, all_entity_label_mask
            )

        entity_preds = (
            np.array(entity_preds) if entity_preds is not None else np.array([])
        )
        all_entity_label_mask = (
            np.array(all_entity_label_mask)
            if all_entity_label_mask is not None
            else np.array([])
        )
        entity_probs = np.array(output[1]) if output[1] is not None else np.array([])
        entity_labels = self._process_predictions(entity_preds, all_entity_label_mask)
        return entity_labels, entity_probs

    @staticmethod
    def load_model() -> EntityExtractionBERT:
        device = get_device()
        if not os.path.exists(Config.SAVE_MODEL_DIR):
            raise Exception("Model doesn't exist! Train first!")

        try:
            model = EntityExtractionBERT.from_pretrained(Config.SAVE_MODEL_DIR)
            total_params = sum(p.numel() for p in model.parameters())
            print(f"Total number of parameters in the model: {total_params}")
            model.to(device)
            model.eval()
            logger.info("***** Model Loaded *****")
        except OSError as e:
            raise Exception("Some model files might be missing...") from e
        except Exception as e:
            raise Exception(
                "An unexpected error occurred while loading the model."
            ) from e

        return model
