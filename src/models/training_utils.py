from typing import Any, Dict, Tuple

import numpy as np


def accumulate_entity_predictions(
    outputs: tuple, batch: list, entity_preds: np.ndarray, entity_labels_ids: np.ndarray
) -> Tuple[np.ndarray, np.ndarray]:
    if entity_preds.size == 0:
        entity_preds = outputs[0].detach().cpu().numpy()
        entity_labels_ids = batch[3].detach().cpu().numpy()
    else:
        entity_preds = np.append(
            entity_preds, outputs[0].detach().cpu().numpy(), axis=0
        )
        entity_labels_ids = np.append(
            entity_labels_ids, batch[3].detach().cpu().numpy(), axis=0
        )
    return entity_preds, entity_labels_ids


def map_entity_labels_and_predictions(
    entity_labels_ids: np.ndarray,
    entity_preds: np.ndarray,
    entity_label_list: list,
    entity_preds_list: list,
    entity_label_map: Dict[int, str],
    pad_label_id: int,
) -> Tuple[list, list]:
    for i in range(entity_labels_ids.shape[0]):
        for j in range(entity_labels_ids.shape[1]):
            if entity_labels_ids[i, j] != pad_label_id:
                entity_label_list[i].append(entity_label_map[entity_labels_ids[i][j]])
                entity_preds_list[i].append(entity_label_map[entity_preds[i][j]])
    return entity_label_list, entity_preds_list


def accumulate_validation_metrics(
    results: dict, valid_batch_metrics: Dict[str, Any]
) -> Dict[str, Any]:
    for key, value in results.items():
        if key == "accuracy":
            valid_batch_metrics[key].append(value)
        else:
            for inner_key, inner_value in value.items():
                valid_batch_metrics[key][inner_key].append(inner_value)
    return valid_batch_metrics


def print_training_validation_metrics(
    train_batch_loss: list, valid_batch_loss: list, results: dict
):
    print(f"train loss функция: {train_batch_loss[-1]}")
    print(f"valid loss функция: {valid_batch_loss[-1]}")
    print(f"macro avg f1: {results['macro avg']['f1-score']}")
