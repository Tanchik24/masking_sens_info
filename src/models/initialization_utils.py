from typing import Any, Dict, List

import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from transformers import AdamW, BertConfig, get_linear_schedule_with_warmup

from src import logger
from src.config import Config
from src.models.EntityExtractionBERT import EntityExtractionBERT
from src.utils import get_entities


def get_device() -> str:
    return "cuda" if torch.cuda.is_available() else "cpu"


def initialize_model_and_config(device: str) -> Dict[str, Any]:
    logger.info("Model initialization started...")
    config = BertConfig.from_pretrained(Config.BERT_PRETRAINED)
    model = EntityExtractionBERT(config=config)
    model.to(device)
    loss_fn = nn.CrossEntropyLoss(ignore_index=Config.IGNORE_INDEX)
    return {"model": model, "config": config, "loss": loss_fn}


def get_optimizer(model: nn.Module) -> optim.Optimizer:
    no_decay = ["bias", "LayerNorm.weight"]
    optimizer_grouped_parameters = [
        dict(
            params=[
                p
                for n, p in model.named_parameters()
                if not any(term in n for term in no_decay)
            ],
            weight_decay=float(Config.WEIGHT_DECAY),
        ),
        {
            "params": [
                p
                for n, p in model.named_parameters()
                if any(term in n for term in no_decay)
            ],
            "weight_decay": 0.0,
        },
    ]
    optimizer = AdamW(
        optimizer_grouped_parameters,
        lr=float(Config.LEARNING_RATE),
        eps=float(Config.ADAM_EPSILON),
    )
    return optimizer


def get_scheduler(
    optimizer: optim.Optimizer, train_dataloader: DataLoader
) -> optim.lr_scheduler.LambdaLR:
    t_total = (
        len(train_dataloader)
        // int(Config.GRADIENT_ACCUMULATION_STEPS)
        * int(Config.NUM_TRAIN_EPOCHS)
    )
    scheduler = get_linear_schedule_with_warmup(
        optimizer, num_warmup_steps=int(Config.WARMUP_STEPS), num_training_steps=t_total
    )
    return scheduler


def create_scheduler_and_optimizer(
    model: nn.Module, train_dataloader: DataLoader
) -> Dict[str, Any]:
    logger.info("Creating scheduler and optimizer...")
    optimizer = get_optimizer(model)
    scheduler = get_scheduler(optimizer, train_dataloader=train_dataloader)
    return {"optimizer": optimizer, "scheduler": scheduler}


def get_counter() -> Dict[str, Any]:
    return {"global_step": 0, "tr_loss": 0.0}


def get_loss_dict() -> Dict[str, List]:
    return {"train": [], "valid": []}


def get_metrics_dict():
    metrcis_dict = {
        entity: {"precision": [], "recall": [], "f1-score": [], "support": []}
        for entity in get_entities()
    }
    metrcis_dict["accuracy"] = []
    metrcis_dict["macro avg"] = {
        "precision": [],
        "recall": [],
        "f1-score": [],
        "support": [],
    }
    metrcis_dict["weighted avg"] = {
        "precision": [],
        "recall": [],
        "f1-score": [],
        "support": [],
    }
    return metrcis_dict
