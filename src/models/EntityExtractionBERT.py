from typing import List

import torch
from transformers import BertModel, BertPreTrainedModel

from src.config import Config
from src.models.EntityClassifier import EntityClassifier
from src.utils import get_entities


class EntityExtractionBERT(BertPreTrainedModel):
    def __init__(self, config, *inputs, **kwargs):
        super().__init__(config, *inputs, **kwargs)
        self.entities: List[str] = get_entities()
        self.num_entities: int = len(self.entities)
        self.bert: BertModel = BertModel.from_pretrained(Config.BERT_PRETRAINED)
        self.entity_classifier: EntityClassifier = EntityClassifier(
            config.hidden_size, self.num_entities, Config.DROPOUT_RATE
        )

    def forward(
        self,
        input_ids: torch.Tensor,
        attention_mask: torch.Tensor,
        token_type_ids: torch.Tensor,
    ):
        outputs = self.bert(
            input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids
        )
        sequence_output = outputs[0]
        entity_logits = self.entity_classifier(sequence_output)
        entity_probs = torch.softmax(entity_logits, dim=-1)
        return entity_logits, entity_probs
