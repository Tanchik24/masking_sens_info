import click

from src.models.Trainer import Trainer


@click.command()
@click.option(
    "--run_name", required=True, help="Run experiment name", type=click.STRING
)
def train_model(run_name: str):
    trainer = Trainer()
    trainer.train(run_name)


if __name__ == "__main__":
    train_model()
