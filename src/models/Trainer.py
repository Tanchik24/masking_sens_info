import os
import pickle
from typing import Any, Dict, List, Tuple, cast

import mlflow
import numpy as np
import pandas as pd
import torch
from mlflow.exceptions import MlflowException
from mlflow.pyfunc import PythonModel, PythonModelContext
from sklearn.metrics import classification_report
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm, trange
from transformers import BertTokenizer

from src import logger, plot_losses
from src.config import Config
from src.features import get_dataloaders_dict
from src.models import (accumulate_entity_predictions,
                        accumulate_validation_metrics,
                        create_scheduler_and_optimizer, get_counter,
                        get_device, get_loss_dict, get_metrics_dict,
                        initialize_model_and_config,
                        map_entity_labels_and_predictions,
                        print_training_validation_metrics)
from src.utils import get_entities, get_params


class ModelWrapper(PythonModel):
    def load_context(self, context: PythonModelContext):
        self._encoder = pickle.load(open(context.artifacts["encoder"], "rb"))
        self._model = pickle.load(open(context.artifacts["model"], "rb"))

    def predict(self, context: PythonModelContext, data):
        for d in data.columns:
            data[d] = self._encoder[d].transform(data[d])
        return self._model.predict(data)


class Trainer:
    def __init__(self):
        self.device: str = get_device()
        self.dataloader: Dict[str, DataLoader] = get_dataloaders_dict()
        self.tokenizer = BertTokenizer.from_pretrained(Config.BERT_PRETRAINED)
        self.entity_label_map: Dict[int, str] = {
            i: label for i, label in enumerate(get_entities())
        }
        self.model_base: Dict[str, Any] = initialize_model_and_config(self.device)
        self.trainers: Dict[str, Any] = create_scheduler_and_optimizer(
            self.model_base["model"], self.dataloader["train"]
        )
        self.counter: Dict[str, Any] = get_counter()
        mlflow.set_tracking_uri(Config.MLFLOW_URL)
        try:
            self.experiment_id = mlflow.create_experiment(
                name=Config.EXPERIMENT_NAME, tags={"version": "1.0.0"}
            )
        except MlflowException:
            existing_experiment = mlflow.get_experiment_by_name(Config.EXPERIMENT_NAME)
            self.experiment_id = existing_experiment.experiment_id

    def get_loss(
        self,
        output: Tuple[torch.Tensor, torch.Tensor],
        attention_mask: torch.Tensor,
        entity_labels_ids: torch.Tensor,
    ) -> torch.Tensor:
        entity_logits = output[0]
        if attention_mask is not None:
            active_loss = attention_mask.view(-1) == 1
            active_logits = entity_logits.view(-1, len(self.entity_label_map))[
                active_loss
            ]
            active_labels = entity_labels_ids.view(-1)[active_loss]
            slot_loss = self.model_base["loss"](active_logits, active_labels)
        else:
            slot_loss = self.model_base["loss"](
                entity_logits.view(-1, len(self.entity_label_map)),
                entity_labels_ids.view(-1),
            )
        return slot_loss

    def batch_step(
        self, batch: list, step: str = "train"
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        batch_tuple = tuple(t.to(self.device) for t in batch)

        inputs = {
            "input_ids": batch_tuple[0],
            "attention_mask": batch_tuple[1],
            "token_type_ids": batch_tuple[2],
        }
        outputs = self.model_base["model"](**inputs)
        loss = self.get_loss(outputs, inputs["attention_mask"], batch_tuple[3])
        if step == "train":
            if Config.GRADIENT_ACCUMULATION_STEPS > 1:
                loss = loss / Config.GRADIENT_ACCUMULATION_STEPS

            loss.backward()
        return loss, outputs

    def optimization_step(self):
        torch.nn.utils.clip_grad_norm_(
            self.model_base["model"].parameters(), Config.MAX_GRAD_NORM
        )

        self.trainers["optimizer"].step()
        self.trainers["scheduler"].step()
        self.model_base["model"].zero_grad()
        self.counter["global_step"] += 1

    def train(self, run_name: str):
        with mlflow.start_run(run_name=run_name, experiment_id=self.experiment_id):
            mlflow.log_params(get_params())
            self.model_base["model"].zero_grad()
            train_iterator = trange(Config.NUM_TRAIN_EPOCHS, desc="Epoch")
            epoch_losses = get_loss_dict()

            for _ in train_iterator:
                epoch_iterator = tqdm(self.dataloader["train"], desc="Iteration")
                batch_losses = get_loss_dict()
                valid_batch_metrics: Dict[str, Any] = get_metrics_dict()

                for step, batch in enumerate(epoch_iterator):
                    loss, output = self.batch_step(batch)

                    self.counter["tr_loss"] += loss.item()

                    if (step + 1) % Config.GRADIENT_ACCUMULATION_STEPS == 0:
                        self.optimization_step()

                        if (
                            Config.LOGGING_STEPS > 0
                            and self.counter["global_step"] % Config.LOGGING_STEPS == 0
                        ):
                            results, valid_loss = self.evaluate()
                            batch_losses["train"].append(loss.item())
                            batch_losses["valid"].append(valid_loss)
                            valid_batch_metrics = accumulate_validation_metrics(
                                results, valid_batch_metrics
                            )
                            print_training_validation_metrics(
                                batch_losses["train"], batch_losses["valid"], results
                            )

                            mlflow.log_metrics(
                                {
                                    "train_loss": loss.item(),
                                    "valid_loss": valid_loss,
                                    "precision": results["macro avg"]["precision"],
                                    "recall": results["macro avg"]["recall"],
                                    "f1-score": results["macro avg"]["f1-score"],
                                },
                                step=self.counter["global_step"],
                            )

                        if (
                            Config.SAVE_STEPS > 0
                            and self.counter["global_step"] % Config.SAVE_STEPS == 0
                        ):
                            self.save_model(Config.SAVE_MODEL_DIR)

                epoch_losses["train"].append(float(np.mean(batch_losses["train"])))
                epoch_losses["valid"].append(float(np.mean(batch_losses["valid"])))

            fig = plot_losses(epoch_losses["train"], epoch_losses["valid"])
            mlflow.log_figure(fig, "training_validation_loss.html")

            example_input_ids = torch.randint(0, 1000, (1, 50))
            example_attention_mask = torch.ones((1, 50), dtype=torch.long)
            example_token_type_ids = torch.zeros((1, 50), dtype=torch.long)

            traced_model = torch.jit.trace(
                self.model_base["model"],
                (example_input_ids, example_attention_mask, example_token_type_ids),
            )

            tokenizer_dir = "./tokenizer"
            os.makedirs(tokenizer_dir, exist_ok=True)
            self.tokenizer.save_pretrained(tokenizer_dir)

            mlflow.log_artifacts(tokenizer_dir, artifact_path="tokenizer")
            mlflow.log_artifact("data/interim/entities.txt", artifact_path="entities")
            artifact_uri = mlflow.get_artifact_uri()
            logger.info(f"artifact_uri: {artifact_uri}")

            model_info = mlflow.pytorch.log_model(
                traced_model, "ner", registered_model_name="ner"
            )
            logger.info({"model_uri": model_info.model_uri})

    def evaluate(self):
        entity_preds: np.ndarray = np.empty((0, 0))
        entity_labels_ids: np.ndarray = np.empty((0, 0))

        batch_loss: List[float] = []
        self.model_base["model"].eval()
        for batch in tqdm(self.dataloader["valid"], desc="Evaluating"):
            loss, output = self.batch_step(batch, "valid")
            batch_loss.append(loss.item())
            entity_preds, entity_labels_ids = accumulate_entity_predictions(
                output, batch, entity_preds, entity_labels_ids
            )

        entity_preds = np.argmax(entity_preds, axis=2)
        entity_label_list: List[List[str]] = [
            [] for _ in range(entity_labels_ids.shape[0])
        ]
        entity_preds_list: List[List[str]] = [
            [] for _ in range(entity_labels_ids.shape[0])
        ]
        entity_label_list, entity_preds_list = map_entity_labels_and_predictions(
            entity_labels_ids,
            entity_preds,
            entity_label_list,
            entity_preds_list,
            self.entity_label_map,
            Config.PAD_LABEL_ID,
        )

        entity_label_list_flat: List[str] = [
            label for sublist in entity_label_list for label in sublist
        ]
        entity_preds_list_flat: List[str] = [
            label for sublist in entity_preds_list for label in sublist
        ]
        report = classification_report(
            entity_label_list_flat, entity_preds_list_flat, output_dict=True
        )
        report_df = pd.DataFrame(report).transpose()
        print(report_df)

        return report, float(np.mean(batch_loss))

    def save_model(self, path) -> None:
        save_model_dir = path
        if not os.path.exists(save_model_dir):
            os.makedirs(save_model_dir)
        model_to_save = (
            self.model_base["model"].module
            if hasattr(self.model_base["model"], "module")
            else self.model_base["model"]
        )
        model_to_save = cast(nn.Module, model_to_save)
        model_to_save.save_pretrained(save_model_dir)
